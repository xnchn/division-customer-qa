# A concept of QA aware deployment
###### Thu Feb  6 13:51:53 CET 2020

## Problem:
- Deployment is chaotic. When the test passes, we are not sure which feature has been tested.
- Unexpected modifications are merged and deployed because cross project development.
- _stage_ is over used for dev test. This causes the feature test unstable.
- There is no guarantee the `thing` tested on _stage_ is the same `thing` to be deployed to _prod_.
- Full feature test on _stage_ takes a lot of time.

## Solution:
- Developers _create_ **release bundle** and _request_ testing on _stage_.
- QA coordinate and _schedule_ (on division level) the *release* to _stage_.
- The *release* need to be reviewed, tested and signed by all parties before goes to _PROD_.
- Ship the same software bundle to _PROD_.

## Work to do:
- Discuss the concept with SM, EM, PO and lead engineers. Get common understanding of the work flow.
- Withdraw the _stage_ deployment right from developers.
- Group E2E tests in [smoke, fast, slow and manually] suites.
- Reduce the E2E test size. Make tests effective.
- Increase test speed by leveraging [test-user-pool](https://bitbucket.org/sixtgoorange/com.sixt.service.test-user-pool/src/master/) service.


![](./release.jpg)

## Parties
- Dev: Developer.
- QA: Quality assurance.
- PO: Product owner. Feature owner.
- RTO: Release train owner. Stakeholders.

## Quality gates explained:
#### Q-0: [Dev]
- No compilation error.
- Code well formatted.

#### Q-1: [Dev]
- Unit test passes.
- Acceptable unit test coverage. (>65%)
- Linter checked.
- Data race checked.
- Unit test report generated.
- README.md updated.
- Code review done. (≥2 approvals)
- Integration test passes.
- Integration test coverage. (>65%)

#### Q-2: [Dev, QA]
- E2E test _smoke suite_ passes.
- E2E test _fast suite_ passes.

#### Q-3: [QA, PO]
- E2E test _smoke suite_ passes.
- E2E test _fast suite_ passes.
- E2E test _slow suite_ passes.
- E2E test _ui suite_ passes(if any).
- Manual test passes(if any).
- PO reviewed.

#### Q-4: [QA, RTO, Customer]
- Manual test passes.
- RTO reviewed.
- Service monitoring available.
- Service healthy

## Fazit
Before I go deeper into practicing these ideas, I would represent this draft version and get feedback from you. The concept diverses fast when thinking the details. But the main target is clear:

> The _stage_ environment should be as close as to the _prod_ environment, likely the deployment process. The expensive E2E tests need to be reduced and bring effectiveness results.

## Reference:
- [Unit, integration and function test explained.](https://codeutopia.net/blog/2015/04/11/what-are-unit-testing-integration-testing-and-functional-testing)
- [Sixt QA community Jan 2020](./200115_QA_Community__final.pdf)

## Alternative automation tools:
- [GoCD](https://www.gocd.org/test-drive-gocd.html)
- [Gauge BDD](https://gauge.org/)
- [Lightning fast CICD](https://buildkite.com/)

---
[Feedback](mailto:xiaoning.cheng@sixt.com)
